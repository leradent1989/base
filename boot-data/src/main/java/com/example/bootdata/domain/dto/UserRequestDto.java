package com.example.bootdata.domain.dto;

import com.example.bootdata.domain.hr.User;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class UserRequestDto {
    @NotNull
    @Min(1)
    @Max(1000)
    private Long id;
    @NotNull
    @Size(min = 3, message = "company name should have at least 3 characters")
    String name;
    private String surname;
    private String address;
    private Date birthDate;
    private String avatar;
    private String header;

}
