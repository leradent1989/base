package com.example.bootdata.domain.dto;

import com.example.bootdata.domain.hr.User;
import lombok.*;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class UserDto {
    private Long id;
    private String name;
    private String surname;
    private String address;
    private Date birthDate;
    private String avatar;
    private String header;



}
