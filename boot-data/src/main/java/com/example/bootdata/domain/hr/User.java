package com.example.bootdata.domain.hr;

import com.example.bootdata.domain.AbstractEntity;
import com.example.bootdata.domain.SysUser;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static jakarta.persistence.TemporalType.TIMESTAMP;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@NamedNativeQuery(
        name = "getAuthUser",
        query = "SELECT * " +
                "FROM users  join  sys_users " +
                "WHERE entity.id = user_id",
        resultClass=User.class
)
@Getter
@Setter
@Entity
@Table(name = "users")
public class User extends AbstractEntity{

   private String name;
   private String surname;
   private String address;
   @Column(name = "birth_date")
   private Date birthDate;
   private String avatar;
   private String header;
   @LazyCollection(LazyCollectionOption.FALSE)
   @JsonIgnore
   @ManyToMany(cascade = { CascadeType.REFRESH })
   @JoinTable(
           name = "followers",
           joinColumns = { @JoinColumn(name = "user_id") },
           inverseJoinColumns = { @JoinColumn(name = "follower_id") })

   private List<User> followers =new ArrayList<>();
   @OneToMany (cascade = {CascadeType.MERGE,CascadeType.REMOVE },fetch = FetchType.EAGER ,mappedBy = "author")
   private List<Post> posts  = new ArrayList<>();
   @OneToMany (cascade = {CascadeType.MERGE,CascadeType.REMOVE },fetch = FetchType.EAGER ,mappedBy = "sender")
   @LazyCollection(LazyCollectionOption.FALSE)
   @JsonIgnore
  private List<Message> messages;


   @Override
   public String toString() {
      return "User{"
              + "name='" + name
              + '\'' + ", surname='"
              + surname + '\''
              + ", address='" + address + '\''
              + ", birthDate=" + birthDate
              + ", avatar='" + avatar
              + '\'' + ", header='"
              + header + '\''
              + ", posts=" + posts
              + '}';
   }
}

