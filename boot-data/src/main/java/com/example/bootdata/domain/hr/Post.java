package com.example.bootdata.domain.hr;

import com.example.bootdata.domain.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.*;


@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
@Entity
@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "posts")
public class Post extends AbstractEntity {

private String text;

    @JsonIgnore
    @ManyToOne(cascade = {CascadeType.PERSIST} ,fetch = FetchType.EAGER )
    @JoinColumn(name = "author_id")
private User author;

// private List <Comment> comments;

    boolean liked;

}

