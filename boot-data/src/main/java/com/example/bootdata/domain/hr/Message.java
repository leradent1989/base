package com.example.bootdata.domain.hr;

import com.example.bootdata.domain.AbstractEntity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Setter
@Getter
@Entity
@Table(name = "messages")

public class Message extends AbstractEntity {


    private String text;


    @ManyToOne(cascade = {CascadeType.PERSIST } ,fetch = FetchType.EAGER )
    @JoinColumn(name = "sender_id")

    private User sender;


    @ManyToOne(cascade = {CascadeType.PERSIST } ,fetch = FetchType.EAGER )
    @JoinColumn(name = "receiver_id")

    private User receiver;

}
