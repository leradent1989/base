package com.example.bootdata.domain.dto;

import com.example.bootdata.domain.hr.User;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class PostDto {

    private Long id;

    private User author;


    private String text;

    private boolean liked;
    protected Date lastModifiedDate;
}
