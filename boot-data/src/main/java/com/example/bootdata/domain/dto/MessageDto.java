package com.example.bootdata.domain.dto;

import lombok.*;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class MessageDto {
    private Long id;

    private String text;
    protected Date lastModifiedDate;
}
