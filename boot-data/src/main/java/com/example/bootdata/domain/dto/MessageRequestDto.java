package com.example.bootdata.domain.dto;

import com.example.bootdata.domain.hr.User;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.validator.constraints.Range;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class MessageRequestDto {
    @NotNull
    @Min(1)
    @Max(1000)
    private Long id;
    private User sender;

    private User receiver;

    private  String  text;

}
