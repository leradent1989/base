package com.example.bootdata.dao;

import com.example.bootdata.domain.hr.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CommentJpaRepository extends JpaRepository<Comment,Long>, JpaSpecificationExecutor<Comment> {
}
