package com.example.bootdata.dao;


import com.example.bootdata.domain.hr.Message;
import com.example.bootdata.domain.hr.User;

import jakarta.persistence.NamedNativeQuery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface UserJpaRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {
 @Query("Select u from User u join SysUser s on u.id = s.userId where s.userName in (:userName)")
  public   User findAuthUser(@Param("userName") String userName);






}
