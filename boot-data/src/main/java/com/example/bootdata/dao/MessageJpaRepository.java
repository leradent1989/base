package com.example.bootdata.dao;



import com.example.bootdata.domain.hr.Message;
import jakarta.persistence.NamedNativeQuery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface MessageJpaRepository extends JpaRepository<Message, Long>, JpaSpecificationExecutor<Message> {

    @Query(value = "select m from Message m where m.sender.id in (:senderId) and m.receiver.id in (:receiverId) "
            + "or  m.sender.id in (:receiverId) and m.receiver.id in (:senderId) ")

    List<Message> findChat(@Param("senderId")Long senderId,@Param("receiverId") Long receiverId);

}
