package com.example.bootdata.service.dtomapper;


import com.example.bootdata.domain.dto.MessageRequestDto;


import com.example.bootdata.domain.hr.Message;
import org.springframework.stereotype.Service;


@Service
public class MessageRequestDtoMapper extends DtoMapperFacade<Message, MessageRequestDto> {

    public MessageRequestDtoMapper() {
        super(Message.class , MessageRequestDto.class);
    }
}