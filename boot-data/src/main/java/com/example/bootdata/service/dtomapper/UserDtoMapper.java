package com.example.bootdata.service.dtomapper;

import com.example.bootdata.domain.dto.UserDto;
import com.example.bootdata.domain.hr.User;
import org.springframework.stereotype.Service;


@Service
    public class UserDtoMapper extends DtoMapperFacade<User, UserDto> {
        public UserDtoMapper() {
            super(User.class , UserDto.class);
        }
    }


