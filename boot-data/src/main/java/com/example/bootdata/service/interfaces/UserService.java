package com.example.bootdata.service.interfaces;

import com.example.bootdata.domain.hr.User;

import java.util.List;
import java.util.UUID;

public interface  UserService {
    List<User> findAll(Integer page, Integer size);

    List<User> findAll();

    void save(User user );

    void update(User user );

    void delete(User user );

    void deleteAll(List<User> userList );

    void saveAll(List<User> userList);

    void deleteById(Long id);

    User getAuthUser(String login);

    User getOne(long id);


}
