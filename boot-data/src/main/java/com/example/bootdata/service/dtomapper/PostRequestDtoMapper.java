package com.example.bootdata.service.dtomapper;


import com.example.bootdata.domain.dto.PostRequestDto;

import com.example.bootdata.domain.hr.Post;
import org.springframework.stereotype.Service;


@Service
public class PostRequestDtoMapper extends DtoMapperFacade<Post, PostRequestDto> {
    public PostRequestDtoMapper() {
        super(Post.class , PostRequestDto.class);
    }
}