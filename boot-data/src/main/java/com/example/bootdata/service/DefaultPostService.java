package com.example.bootdata.service;




import com.example.bootdata.dao.UserJpaRepository;
import com.example.bootdata.dao.PostJpaRepository;
import com.example.bootdata.domain.hr.User;
import com.example.bootdata.domain.hr.Post;
import com.example.bootdata.service.interfaces.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class DefaultPostService implements PostService {

    private final PostJpaRepository postRepository;




    @Override
    public List<Post> findAll(Integer  page, Integer size) {

        Sort sort =  Sort.by(new Sort.Order(Sort.Direction.ASC,"id"));
        Pageable pageable = PageRequest.of(page,size,sort);
        Page<Post> departmentPage = postRepository.findAll(pageable);

        return departmentPage.toList();

    }

    @Transactional(readOnly = true)
    @Override
    public List<Post> findAll() {
        return postRepository.findAll();
    }

    @Override
    public void save(Post post) {
        postRepository.save(post ) ;
    }

    @Override
    public void update(Post post) {

        post.setCreationDate(postRepository.getOne(post.getId()).getCreationDate());
        post.setAuthor(postRepository.getOne(post.getId()).getAuthor());
        postRepository.save(post) ;
    }

    @Override
    public void delete(Post customer) {

      postRepository.delete(customer);
    }

    @Override
    public void deleteAll(List<Post> customers) {
         postRepository.deleteAll(customers ) ;
    }

    @Override
    public void saveAll(List<Post> customers) {
           postRepository.saveAll(customers);
    }


    @Override
    public void deleteById(Long id) {

       postRepository.deleteById(id) ;
    }

    @Transactional(readOnly = true)
    @Override
    public Post getOne(Long id) {
        return postRepository.getOne(id);
    }


}
