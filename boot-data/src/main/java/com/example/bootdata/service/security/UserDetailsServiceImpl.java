package com.example.bootdata.service.security;



import com.example.bootdata.dao.UserJpaRepository;
import com.example.bootdata.domain.SysRole;
import com.example.bootdata.domain.SysUser;
import com.example.bootdata.repository.SysUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
    private final SysUserRepository userRepository;
    private final UserJpaRepository repository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<SysUser> sysUser = userRepository.findUsersByUserName(username);
        if (sysUser.isEmpty()) {
            throw new UsernameNotFoundException(username);
        }
        List<SimpleGrantedAuthority> authorities = sysUser.get().getSysRoles().stream()
                .map(r -> new SimpleGrantedAuthority(r.getRoleName()))
                .toList();
        return new User(sysUser.get().getUserName(), sysUser.get().getEncryptedPassword(), authorities);

    }

    public void createNew(String userName, String password) {
        SysRole role = new SysRole(null, "USER", null);
        SysUser user = new SysUser(null,
                userName,
                passwordEncoder.encode(password),
                true,
                Set.of(role));
        role.setSysUser(user);
        com.example.bootdata.domain.hr.User newUser = new com.example.bootdata.domain.hr.User();
        userRepository.save(user);
        repository.save(newUser);
    }

    public List<SysUser> findAll() {
        return userRepository.findAll();
    }





}

