package com.example.bootdata.service.interfaces;

import com.example.bootdata.domain.hr.User;
import com.example.bootdata.domain.hr.Post;

import java.util.List;

public interface PostService {
    List<Post> findAll(Integer page, Integer size);

    List<Post> findAll();

    void save(Post customer );

    void update(Post customer);

    void delete(Post customer);

    void deleteAll(List<Post> customers);

    void saveAll(List<Post> customers);

    void  deleteById(Long id);

    Post getOne(Long id);




}
