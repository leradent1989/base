package com.example.bootdata.service;


import com.example.bootdata.dao.UserJpaRepository;
import com.example.bootdata.domain.hr.User;
import com.example.bootdata.service.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;


import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class DefaultUserService implements UserService {

    private final UserJpaRepository userRepository;

    @Override
    public List<User> findAll(Integer  page, Integer size) {

        Sort sort =  Sort.by(new Sort.Order(Sort.Direction.ASC,"id"));
        Pageable pageable = PageRequest.of(page,size,sort);
        Page<User> departmentPage = userRepository.findAll(pageable);

        return departmentPage.toList();

    }

    @Transactional(readOnly = true)
    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }


    @Transactional(readOnly = true)
    @Override
    public User getOne(long id) {
        return userRepository.getOne(id);
    }

    @Override
    public void save(User user) {
        user.setCreationDate(userRepository .getOne(user.getId()).getCreationDate());
        userRepository.save(user);
    }

    @Override
    public void update(User user) {

        user.setCreationDate(userRepository .getOne(user.getId()).getCreationDate());
        user.setAvatar(userRepository .getOne(user.getId()).getAvatar());
        user.setHeader(userRepository .getOne(user.getId()).getHeader());
        user.setFollowers(userRepository .getOne(user.getId()).getFollowers());


        userRepository.save(user);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public void deleteAll(List<User> userList) {
        userRepository.deleteAll(userList );
    }

    @Override
    public void saveAll(List<User> userList) {
        userRepository.saveAll(userList);
    }


    @Override
    public void deleteById(Long id) {
        userRepository.deleteById(id);

    }
@Override
    public User getAuthUser(String login){

        return userRepository.findAuthUser(login);
}

}
