package com.example.bootdata.resource;

import com.example.bootdata.domain.dto.*;
import com.example.bootdata.domain.hr.User;
import com.example.bootdata.domain.hr.Post;

import com.example.bootdata.service.interfaces.PostService;

import com.example.bootdata.service.interfaces.UserService;
import com.example.bootdata.service.dtomapper.*;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/posts")
@CrossOrigin( origins = {"http://localhost:3000"})
public class PostRestController {

    private final PostService postService;
    private final UserService userService ;

    private final PostDtoMapper dtoMapper;

    private final PostRequestDtoMapper requestMapper;

    private  final UserDtoMapper userMapper;


    @GetMapping

    public List<PostDto> findAll() {
        return postService.findAll(0,10).stream().map(dtoMapper::convertToDto).collect(Collectors.toList());

    }

    @GetMapping("/{page}/{size}")
    public ResponseEntity<?> findAll(@PathVariable Integer page, @PathVariable Integer size) {
        List<Post> departments = postService.findAll(page, size);
        List<PostDto> departmentsDto = departments.stream().map(dtoMapper::convertToDto).collect(Collectors.toList());

        return ResponseEntity.ok(departmentsDto);
    }

    @GetMapping("/{id}/author")
    public ResponseEntity<?> getAuthor(@PathVariable ("id") Long postId) {
        Post post = postService.getOne(postId);
        User postAuthor = post.getAuthor();

        if (post == null) {
            return ResponseEntity.badRequest().body("Post not found");
        }
       UserDto postAuthorDto = userMapper.convertToDto(postAuthor);
        return ResponseEntity.ok().body(postAuthorDto );
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable ("id") Long customerId) {
        Post customer = postService.getOne(customerId);
        if (customer  == null) {
            return ResponseEntity.badRequest().body("Post not found");
        }

        return ResponseEntity.ok().body(dtoMapper.convertToDto(customer) );
    }

    @PostMapping
    public void create( @Valid  @RequestBody PostRequestDto customer ) {
       postService.save(requestMapper.convertToEntity(customer) );
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody PostRequestDto customerRequestDto ) {
        try {

        postService.update(requestMapper.convertToEntity(customerRequestDto));

      return ResponseEntity.ok().build();

        } catch (RuntimeException e) {

            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable ("id") Long customerId) {
        try {
            postService.deleteById(customerId);
            return ResponseEntity.ok().build();
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }

      @DeleteMapping

    public ResponseEntity<?> deleteCustomer(@RequestBody PostRequestDto customer) {

        try {
            postService.delete(requestMapper.convertToEntity(customer));
         return    ResponseEntity.ok().build();

        } catch (RuntimeException e) {

            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }





}