package com.example.bootdata.resource;

import com.example.bootdata.dao.UserJpaRepository;
import com.example.bootdata.domain.dto.UserDto;
import com.example.bootdata.domain.dto.UserRequestDto;


import com.example.bootdata.domain.hr.User;

import com.example.bootdata.service.interfaces.UserService;

import com.example.bootdata.service.dtomapper.UserDtoMapper;
import com.example.bootdata.service.dtomapper.UserRequestDtoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
@CrossOrigin(origins = {"http://localhost:3000"})
public class UserRestController {
    private final UserService userService;

    private final UserDtoMapper dtoMapper;
    private final UserRequestDtoMapper requestMapper;

    private final UserJpaRepository repository;

    @GetMapping
    public List<UserDto> getAll() {
        return userService.findAll(0,10).stream().map(dtoMapper::convertToDto).collect(Collectors.toList());
    }

    @GetMapping("/{page}/{size}")

    public ResponseEntity<?> findAll(@PathVariable Integer page, @PathVariable Integer size) {
        List<User> users = userService.findAll(page, size);
        List<UserDto> userDtoList = users.stream().map(dtoMapper::convertToDto).collect(Collectors.toList());

        return ResponseEntity.ok(userDtoList);
    }
    @GetMapping("/auth/{login}")
    public ResponseEntity <?> getAuthUser(@PathVariable ("login") String name){


      return ResponseEntity.ok(dtoMapper.convertToDto(userService.getAuthUser(name))) ;
    }

    @GetMapping("/{id}")

    public ResponseEntity<?>  getById(@PathVariable("id")  Long  userId) {
       User user = userService.getOne(userId );

        if (user   == null) {
            return ResponseEntity.badRequest().body("Employer not found");
        }
        return ResponseEntity.ok().body(dtoMapper.convertToDto(user) );
    }

    @GetMapping("/{id}/followers")

    public ResponseEntity<?>  getFollowers(@PathVariable("id")  Long  userId) {
        User user = userService.getOne(userId );

        if (user == null) {
            return ResponseEntity.badRequest().body("Employer not found");
        }
        List<User> followers = user.getFollowers();
        return ResponseEntity.ok().body(followers );
    }

    @PostMapping

    public void create(@RequestBody UserRequestDto user ) {
        userService.save(requestMapper.convertToEntity(user) );
    }

    @DeleteMapping("/{id}")

    public ResponseEntity<?> deleteById(@PathVariable("id")Long userId) {
        try {
         userService.deleteById(userId);
            return ResponseEntity.ok().build();
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @DeleteMapping

    public ResponseEntity<?> deleteEmployee(@RequestBody UserRequestDto company) {
        try {
            userService.delete(requestMapper.convertToEntity(company));
            return ResponseEntity.ok().build();
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @PutMapping("/{userId}/{id}")

    public ResponseEntity<?> updateFollowers(@PathVariable Long userId,@PathVariable Long id) {
        try {
            User userEntity = userService.getOne(userId);

            List <User> followers = userEntity.getFollowers();
            System.out.println(followers);


            followers.add(userService.getOne(id));
            User follower = userService.getOne(id);
            List <User> followerFollowers = follower.getFollowers();
            followerFollowers.add(userService.getOne(userId));




            userEntity.setFollowers(followers);
            follower.setFollowers(followerFollowers);

            userService.save(userEntity);
            userService.save(follower);


            return ResponseEntity.ok().build();
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @PutMapping("unfollow/{userId}/{id}")

    public ResponseEntity<?> removeFollowers(@PathVariable Long userId,@PathVariable Long id) {
        try {
            User userEntity = userService.getOne(userId);

            List <User> followers = userEntity.getFollowers();
            System.out.println(followers);


              followers.removeIf(el ->el.getId().equals(id));
              User follower = userService.getOne(id);
              List <User> followerFollowers = follower.getFollowers();
              followerFollowers.removeIf(el ->el.getId().equals(userId));




            userEntity.setFollowers(followers);
            follower.setFollowers(followerFollowers);

            userService.save(userEntity);
            userService.save(follower);

            return ResponseEntity.ok().build();
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @PutMapping

    public ResponseEntity<?> update(@RequestBody UserRequestDto employee) {
        try {
            userService.update(requestMapper.convertToEntity(employee));
            return ResponseEntity.ok().build();
        } catch (RuntimeException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }


}
