package com.example.bootdata.controller;

import com.example.bootdata.domain.SysUser;
import com.example.bootdata.domain.dto.SysUserDto;
import com.example.bootdata.service.security.UserDetailsServiceImpl;
import com.example.bootdata.service.dtomapper.SysUserDtoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequiredArgsConstructor
@CrossOrigin(origins = {"http://localhost:3000"})
public class UserController {
    private final UserDetailsServiceImpl userDetailsService;
    private final SysUserDtoMapper userDtoMapper;

    // For basic auth
    @GetMapping("/dashboard")
    public String basicAuth(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();

        model.addAttribute("employees", new String[]{"ddff"});
        printSecurityUserName();

    return "dashboard";
    }
@GetMapping("/auth")


    private ResponseEntity<?> printSecurityUserName() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
System.out.println(name);

 userDetailsService.findAll().stream().forEach(el ->System.out.println(el.getUserName().equals(name)));

        System.out.println(name);
        return ResponseEntity.ok(name);}

    @GetMapping("/login")

    public String userDetails(Model model) {
        model.addAttribute("employees", new String[]{"ddff"});

        return "/login";
    }


    @GetMapping("/registration")

    public String reg() {
        return "registration";
    }

    @PostMapping("/registration")

    public String registrate(@RequestParam String username, @RequestParam String password ) {
        userDetailsService.createNew(username, password);


        return "/login";
    }

    @GetMapping("/sys_users")
    @ResponseBody

    public List<SysUserDto> getAll() {
        return userDetailsService.findAll().stream()
                .map(userDtoMapper::convertToDto)
                .toList();
    }


}
