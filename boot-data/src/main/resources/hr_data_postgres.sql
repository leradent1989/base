--create tables
BEGIN;


INSERT INTO public.users ( entity_id,name,surname,address,birth_date,avatar,header,creation_date,last_modified_date) VALUES
                                                                                                                         (100,'Alex', ' Smith','4Avenue',TO_DATE('25-JUl-1977', 'dd-MON-yyyy'),'https://qph.cf2.quoracdn.net/main-qimg-ed424b4d548229863a57603462976e3e.webp' ,'https://photographylife.com/wp-content/uploads/2017/01/Best-of-2016-Nasim-Mansurov-20.jpg',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')) ,
                                                                                                                         (101,'Cris ', 'Thomson','56Payette Lane',TO_DATE('25-AUG-1986', 'dd-MON-yyyy'), 'https://assets.thehansindia.com/h-upload/2020/05/16/969648-k-pop-singer-bts-v-most-han.webp','https://ichef.bbci.co.uk/news/999/cpsprodpb/6D5A/production/_119449972_10.jpg',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                                                         (102,'Roger ','Williams','78Mansfield Ave',TO_DATE('15-APR-1997', 'dd-MON-yyyy'),'https://www.thecoldwire.com/wp-content/uploads/2021/11/Closeup-portrait-of-a-handsome-man-at-gym.jpeg','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7SPMxYYTAmSxcMRvEIwePcBNpJi9eEdEM9A&usqp=CAU',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy'));


INSERT INTO sys_users(user_id, enabled, encrypted_password, user_name/*,sys_id*/) VALUES
                                                                                      (100, true, '$2a$10$BXH1wlAJPIMXvjnJTBoRuea4CvZwSs8/Zqz4bDRZBDJ6hxvXoHlqq', 'a'),
                                                                                      (101, true, '$2a$10$BXH1wlAJPIMXvjnJTBoRuea4CvZwSs8/Zqz4bDRZBDJ6hxvXoHlqq', 'admin'),
                                                                                      (102, true, '$2a$10$BXH1wlAJPIMXvjnJTBoRuea4CvZwSs8/Zqz4bDRZBDJ6hxvXoHlqq', 'test');




INSERT INTO public.posts (entity_id,author_id,text,liked,creation_date,last_modified_date) VALUES
                                                                                               (100,102,'trgkrogrg',0,TO_DATE('25-JUN-2017', 'dd-MON-yyyy'),TO_DATE('25-JUN-2020', 'dd-MON-yyyy')),
                                                                                               (101,100,'grgrawe',0,TO_DATE('25-JUN-2017', 'dd-MON-yyyy'),TO_DATE('25-JUN-2020', 'dd-MON-yyyy')),
                                                                                               (102,101, 'efrmfsws',0,TO_DATE('25-JUN-2021', 'dd-MON-yyyy'),TO_DATE('25-JUN-2021', 'dd-MON-yyyy')),
                                                                                               (103,100,'frgrg5wws',0,TO_DATE('25-JUN-2022', 'dd-MON-yyyy'),TO_DATE('25-JUN-2022', 'dd-MON-yyyy')),
                                                                                               (104,102,'trtrgmgse',0,TO_DATE('25-JUN-2021', 'dd-MON-yyyy'),TO_DATE('25-JUN-2021', 'dd-MON-yyyy')),
                                                                                               (105,101,'dtrtrtrmvdwe',0,TO_DATE('25-JUN-2022', 'dd-MON-yyyy'),TO_DATE('25-JUN-2022', 'dd-MON-yyyy')),
                                                                                               (106,100,'ddffgkrksmxme',0,TO_DATE('25-JUN-2019', 'dd-MON-yyyy'),TO_DATE('25-JUN-2021', 'dd-MON-yyyy'));







INSERT INTO public.messages (entity_id,sender_id,receiver_id,text,creation_date,last_modified_date) VALUES

                                                                                                        (200,102,101,'Hy',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                                        (201,101,100,'Hello',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                                        (202,101,100,'Hy',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                                        (203,102,100,'Hello',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                                        (204,100,102,'Hy',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                                        (205,101,102,'Hello',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy')),
                                                                                                        (206,101,100,'Hy',TO_DATE('25-JUN-1997', 'dd-MON-yyyy'),TO_DATE('25-JUN-1997', 'dd-MON-yyyy'));

INSERT INTO public.followers (id,user_id,follower_id) VALUES

                                                          (81,100,101),
                                                          (82,100,102),
                                                          (83,101,100),
                                                          (84,101,102),
                                                          (85,102,100),
                                                          (86,102,101);


INSERT INTO roles(role_id, role_name, user_id) VALUES
                                                   (101, 'USER', 101),
                                                   (102, 'ADMIN', 102),
                                                   (103,'USER',103 );



COMMIT;

