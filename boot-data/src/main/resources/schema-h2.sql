DROP TABLE IF EXISTS students;
DROP TABLE IF EXISTS public.users  CASCADE ;
CREATE TABLE public.users (
                              entity_id INT AUTO_INCREMENT PRIMARY KEY,
                              name VARCHAR(250) NOT NULL,
                              surname  VARCHAR (250) NOT NULL,
                              address VARCHAR (250) NOT NULL,
                              birth_date TIMESTAMP NOT NULL ,
                              avatar VARCHAR (250) NOT NULL ,
                              header VARCHAR (250) NOT NULL ,
                              /*  auth_id INT NOT NULL ,
                              FOREIGN KEY (auth_id) RERENCES sys_users(user_id),*/
                              creation_date TIMESTAMP NOT NULL ,
                               last_modified_date TIMESTAMP NOT NULL
);

DROP TABLE IF EXISTS sys_users CASCADE;
CREATE TABLE sys_users (
user_id INT AUTO_INCREMENT  PRIMARY KEY,
user_name VARCHAR(36) NOT NULL,
encrypted_password VARCHAR(128) NOT NULL,
/* sys_id INT NOT NULL ,
FOREIGN KEY (sys_id) REFERENCES users(entity_id),*/
creation_date TIMESTAMP  NULL,
last_modified_date TIMESTAMP  NULL,
created_by INT NULL,
last_modified_by INT NULL,
enabled boolean NOT NULL
);
/*DROP TABLE IF EXISTS user_sys_user CASCADE ;
CREATE TABLE user_sys_user
(
    id SERIAL PRIMARY KEY ,
    user_id INT NOT NULL,
    sys_user_id INT NOT NULL ,
    FOREIGN KEY (user_id) REFERENCES users(entity_id),
    FOREIGN KEY (sys_user_id) REFERENCES sys_users (user_id)



);*/




DROP TABLE IF EXISTS public.messages CASCADE ;
CREATE TABLE public.messages (
   entity_id INT AUTO_INCREMENT PRIMARY KEY,
    sender_id INTEGER references users(entity_id) ,
   receiver_id INTEGER references users(entity_id),
   text VARCHAR (250) NOT NULL,
   creation_date TIMESTAMP NOT NULL ,
   last_modified_date TIMESTAMP NOT NULL

);
DROP TABLE IF EXISTS public.posts CASCADE ;
CREATE TABLE public.posts(
entity_id INT AUTO_INCREMENT PRIMARY KEY ,
author_id INTEGER references users(entity_id),
text  VARCHAR(280) NOT NULL,
liked BIT NOT NULL ,
creation_date TIMESTAMP NOT NULL ,
last_modified_date TIMESTAMP NOT NULL

);
DROP TABLE IF EXISTS public.followers CASCADE ;

CREATE TABLE followers
(
id SERIAL PRIMARY KEY ,
user_id INT NOT NULL,
follower_id INT NOT NULL ,
FOREIGN KEY (user_id) REFERENCES users(entity_id),
FOREIGN KEY (follower_id) REFERENCES users(entity_id)



);



DROP TABLE IF EXISTS roles CASCADE;



CREATE TABLE roles (
role_id INT AUTO_INCREMENT  PRIMARY KEY,
role_name VARCHAR(30) NOT NULL,
user_id INT,
);
