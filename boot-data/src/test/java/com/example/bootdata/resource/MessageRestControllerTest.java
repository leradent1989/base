package com.example.bootdata.resource;

import com.example.bootdata.dao.UserJpaRepository;
import com.example.bootdata.dao.PostJpaRepository;
import com.example.bootdata.domain.SysRole;
import com.example.bootdata.domain.SysUser;
import com.example.bootdata.domain.dto.MessageDto;
import com.example.bootdata.domain.hr.User;
import com.example.bootdata.repository.SysUserRepository;
import com.example.bootdata.service.dtomapper.*;
import com.example.bootdata.service.interfaces.PostService;
import com.example.bootdata.service.security.CustomOAuth2UserService;
import com.example.bootdata.service.security.UserDetailsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.security.test.context.support.WithMockUser;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(MessageRestController.class)

public class MessageRestControllerTest {

    @Autowired
    private MockMvc mockMvc;


    @MockBean
    private PostJpaRepository customerJpaRepository;
@MockBean
private UserJpaRepository accountJpaRepository;
    @MockBean

    private SysUserRepository userRepository ;
    @MockBean
    private PostService customerService;
    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private CustomOAuth2UserService oauthUserService;

    @MockBean
    private PostDtoMapper dtoMapper;
    @MockBean
    private PostRequestDtoMapper requestMapper;


    @MockBean
    private MessageDtoMapper employerMapper;

    @TestConfiguration
    public static class TestConfig {
        @Bean // Тестируем компонентно с меппером
        public UserDtoMapper employeeDtoMapper() {
            return new UserDtoMapper();
        }
    }
    @BeforeEach
    public void setUp()  throws Exception  {
        SysUser user = new SysUser(1L, "1", "1", true, Set.of(new SysRole(1L, "USER", null)));
        when(userDetailsService.findAll()).thenReturn(List.of(user));

    }
    @Test
    @WithMockUser(value = "a")

    public void getById() throws Exception {


    }

    @Test
    @WithMockUser(value = "user1")
    public void findAll() throws Exception {



    }
    @Test
    @WithMockUser(value = "user1")
    public void findAllPageable() throws Exception {



    }

    @Test
    @WithMockUser(value = "user1")
    public void testCreate() throws Exception {


        this.mockMvc.perform(MockMvcRequestBuilders.post("/messages")
                        .contentType("application/json")
                        .content(
                                """
                               {
                                
                                 }
                                """
                        ))
                .andExpect(status().isOk())

        ;
    }
    @Test
    @WithMockUser(value = "user1")


    public void testPut() throws Exception {

        this.mockMvc.perform(MockMvcRequestBuilders.put("/messages")
                        .contentType("application/json")
                        .content(
                                """
                               {
                                
                                 }
                                """
                        ))
                .andExpect(status().isOk())

        ;
    }
    @Test
    @WithMockUser(value = "user1")

    public void testDelete() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/messages")
                        .contentType("application/json")
                        .content(
                                """
                                
                                   {
                                          
                                             }
                                   
                                
                                """
                        ))
                .andExpect(status().isOk())

        ;
    }
    @Test
    @WithMockUser(value = "user1")

    public void testDeleteById() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/messages/116").contentType("application/json"))
                .andExpect(status().isOk())


        ;
    }


}