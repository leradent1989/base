package com.example.bootdata.resource;


import com.example.bootdata.dao.MessageJpaRepository;
import com.example.bootdata.dao.PostJpaRepository;
import com.example.bootdata.domain.SysRole;
import com.example.bootdata.domain.SysUser;

import com.example.bootdata.domain.hr.User;
import com.example.bootdata.repository.SysUserRepository;
import com.example.bootdata.service.dtomapper.*;
import com.example.bootdata.service.interfaces.MessageService;
import com.example.bootdata.service.interfaces.PostService;
import com.example.bootdata.service.security.CustomOAuth2UserService;
import com.example.bootdata.service.security.UserDetailsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.security.test.context.support.WithMockUser;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PostRestController.class)

public class PostRestControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CustomOAuth2UserService oauthUserService;

    @MockBean
    private PostJpaRepository customerJpaRepository;
    @MockBean
    private MessageJpaRepository employerJpaRepository;
    @MockBean

    private SysUserRepository userRepository ;
    @MockBean
    private PostService customerService;
    @MockBean
    private UserDetailsServiceImpl userDetailsService;


    @MockBean
    private MessageService employerService;

    @MockBean
    private PostRequestDtoMapper requestMapper;
    @MockBean
    private PostDtoMapper accountMapper;
    @MockBean
    private MessageDtoMapper employerMapper;
    @MockBean
    private MessageRequestDtoMapper requestEmployerMapper;
    @TestConfiguration
    public static class TestConfig {
        @Bean // Тестируем компонентно с меппером
        public UserDtoMapper employeeDtoMapper() {
            return new UserDtoMapper();
        }
    }
    @BeforeEach
    public void setUp()  throws Exception  {
        SysUser user = new SysUser(1L, "1", "1", true, Set.of(new SysRole(1L, "USER", null)));
        when(userDetailsService.findAll()).thenReturn(List.of(user));

    }
    @Test
    @WithMockUser(value = "user1")

    public void getById() throws Exception {

    }

    @Test
    @WithMockUser(value = "user1")
    public void findAll() throws Exception {



    }
    @Test
    @WithMockUser(value = "user1")
    public void findAllPageable() throws Exception {


    }

    @Test
    @WithMockUser(value = "user1")
    public void testCreate() throws Exception {


        this.mockMvc.perform(MockMvcRequestBuilders.post("/posts")
                        .contentType("application/json")
                        .content(
                                """
                               {
                                
                                 }
                                """
                        ))
                .andExpect(status().isOk())

        ;
    }
    @Test
    @WithMockUser(value = "user1")


    public void testPut() throws Exception {

        this.mockMvc.perform(MockMvcRequestBuilders.put("/posts")
                        .contentType("application/json")
                        .content(
                                """
                               {
                                 
                                 }
                                """
                        ))
                .andExpect(status().isOk())

        ;
    }
    @Test
    @WithMockUser(value = "user1")

    public void testDelete() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/posts")
                        .contentType("application/json")
                        .content(
                                """
                                
                                   {
                                              
                                             
                                             }
                                   
                                
                                """
                        ))
                .andExpect(status().isOk())

        ;
    }
    @Test
    @WithMockUser(value = "user1")

    public void testDeleteById() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/posts/205").contentType("application/json"))
                .andExpect(status().isOk())


        ;
    }
}