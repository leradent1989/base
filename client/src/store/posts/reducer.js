import { setPosts } from "./actions";
import {likePost} from "./actions";
import {dislikePost} from "./actions";


const initialValue = {
    value: [],
    favorites:[],
    isLoading: true
}

const PostsReducer = (state = initialValue, action) => {
    switch (action.type) {


        case setPosts: {
            return {...state, value: action.payload, isLoading: false }
        }
        case likePost: {
            let postId = action.payload.id
            let posts = state.value;
            let post = posts.filter(el => el.id === postId)
            console.log(post.text)
            post.liked = true;



            fetch(`http://localhost:9000/posts`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({

                    id: postId,

                    text: post[0].text,
                    liked: true

                })
            })

            return {...state, isLoading: false }
        }
        case dislikePost: {
            let postId = action.payload.id
            let posts = state.value;
            let post = posts.filter(el => el.id === postId)
            console.log(post.text)
            post.liked = false;



            fetch(`http://localhost:9000/posts`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({

                    id: postId,

                    text: post[0].text,
                    liked: false

                })
            })

            return {...state, isLoading: false }
        }
        default: {
            return state
        }


    }

}
export default PostsReducer;