import { setPosts} from "./actions";
import {likePost} from "./actions";
import {dislikePost} from "./actions";


export const setPostsAC = () => async (dispatch) => {

    try {
        //const { status, data }
        const result    = await fetch('http://localhost:9000/posts').then(response => response.json());


        //  if (status === 'success') {


        dispatch({ type: setPosts, payload: result })

        //  }

    } catch (err) {

        console.log(err);
    }
}
export const likePostAC = (payload) => ({ type: likePost,payload })

export const dislikePostAC = (payload) => ({ type: dislikePost,payload })