import { addFollower, setFollowers, removeFollower } from "./actions";



export const setFollowersAC = (userId) => async (dispatch) => {

    try {
        //const { status, data }
        const result    = await fetch(`http://localhost:9000/users/${userId}/followers`).then(response => response.json());

        console.log(result)
        //  if (status === 'success') {


        dispatch({ type: setFollowers, payload: result })

        //  }

    } catch (err) {

        console.log(err);
    }
}

export const addFollowerAC = (payload) => ({ type: addFollower, payload })

export const removeFollowerAC = (payload) => ({ type: removeFollower, payload })
