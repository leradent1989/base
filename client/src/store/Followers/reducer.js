import {addFollower,removeFollower} from "./actions";
import {setFollowers} from "../users/actions";

const initialValue = {
  value: [],
  isLoading: true,

}
const FollowerReducer = (state = initialValue, action) => {

  switch (action.type) {
    case setFollowers: {
      return {...state, value: action.payload, isLoading: false }
    }

    case addFollower: {
      let follower = action.payload.follower;


      let authUser = action.payload.auth;




      fetch(`http://localhost:9000/users/${follower.id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ authUser})
      })
      return {...state, isLoading: false }

    }
    case removeFollower: {

      let follower = action.payload.follower;

      let authUser = action.payload.auth;






      fetch(`http://localhost:9000/users/unfollow/${authUser.id}/${follower.id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(authUser)
      })
      return {...state, isLoading: false }

    }

    default: {
      return state
    }

  }


}
export default FollowerReducer;

