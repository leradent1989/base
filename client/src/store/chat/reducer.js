import {setChatMessages} from "./actions";


const initialValue = {

    value:[],
    isLoading: true
}

const MessagesReducer = (state = initialValue, action) => {
    switch (action.type) {


        case setChatMessages: {
            return { value: action.payload, isLoading: false }
        }

        default: {
            return state
        }


    }

}
export default MessagesReducer;