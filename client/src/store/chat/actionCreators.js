import { setChatMessages } from "./actions";



export const setChatMessagesAC = (id1,id2) => async (dispatch) => {

    try {
        //const { status, data }
        const result    = await fetch(`http://localhost:9000/messages/${id1}/${id2}/chat`).then(response => response.json());

        console.log(result)
        //  if (status === 'success') {


        dispatch({ type: setChatMessages, payload: result })

        //  }

    } catch (err) {

        console.log(err);
    }

}