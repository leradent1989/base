import { addComments, setComments, showComments } from "./actions";

const initialValue = {
    value: [],
    isLoading: true,

}


const commentsReducer = (state = initialValue, action) => {

    switch (action.type) {
        case setComments: {

            return { value: action.payload, isLoading: false }
        }

        case addComments: {


        }

        case showComments: {


        }

        default: {
            return state
        }


    }


}

export default commentsReducer;


