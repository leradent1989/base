import { setUsers } from "./actions";
import {setFollowers} from "./actions";
import {setAuth} from "./actions";
import {logOut} from "./actions";

const initialValue = {
    value: [],
    followers:[],
    auth:JSON.parse(localStorage.getItem("login")),
    isLoading: true
}

const UsersReducer = (state = initialValue, action) => {
    switch (action.type) {


        case setUsers: {
            return {...state, value: action.payload, isLoading: false }
        }
        case setFollowers: {
            return {...state, followers: action.payload, isLoading: false }
        }
        case setAuth: {
            return {...state, auth:action.payload, isLoading: false }
        }
        case logOut: {
            return {...state, auth:action.payload, isLoading: false }
        }
        default: {
            return state
        }


    }

}
export default UsersReducer;