import { setUsers } from "./actions";
import {setFollowers } from "./actions";
import {setAuth} from "./actions";
import  {logOut} from "./actions";

export const setUsersAC = () => async (dispatch) => {

    try {
        //const { status, data }
        const result    = await fetch('http://localhost:9000/users').then(response => response.json());

console.log(result)
      //  if (status === 'success') {


            dispatch({ type: setUsers, payload: result })

      //  }

    } catch (err) {

        console.log(err);
    }
}
export const setFollowersAC = (userId) => async (dispatch) => {

    try {
        //const { status, data }
        const result    = await fetch(`http://localhost:9000/users/${userId}/followers`).then(response => response.json());

        console.log(result)
        //  if (status === 'success') {


        dispatch({ type: setFollowers, payload: result })

        //  }

    } catch (err) {

        console.log(err);
    }
}
export const setAuthAC = (login) => async (dispatch) => {

    try {
        //const { status, data }
        const result    = await fetch(`http://localhost:9000/users/auth/${login}`).then(response => response.json());

        console.log(result)
        //  if (status === 'success') {
  localStorage.setItem("login",JSON.stringify(result))

        dispatch({ type: setAuth, payload: result })

        //  }

    } catch (err) {

        console.log(err);
    }
}
export const logOutAC = () => async (dispatch) => {

    try {
        //const { status, data }
        const result    = await fetch(`http://localhost:9000/logout`).then(result => console.log(result))


        //  if (status === 'success') {
        localStorage.removeItem("login")

        dispatch({ type: logOut, payload: null })

        //  }

    } catch (err) {

        console.log(err);
    }
}