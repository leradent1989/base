import { combineReducers } from "redux";

import modalReducer from './modal/reducer'

import UsersReducer from "./users/reducer";
import FollowerReducer from "./Followers/reducer";

import commentsReducer from "./comments/reducer";

import MessagesReducer from "./chat/reducer";

import PostsReducer from "./posts/reducer";


const appReducer = combineReducers({
   modal: modalReducer,
   users: UsersReducer,
   followers: FollowerReducer,
   comments: commentsReducer,
   messages:MessagesReducer,
   posts:PostsReducer

})
export default appReducer;