import './App.scss';
import React, { useEffect,useState} from 'react';
import Header from "./components/header/Header";
import AppRoutes from './AppRoutes';
import { useSelector } from 'react-redux'
import { useDispatch } from 'react-redux'
import {setAuthAC, setUsersAC} from "./store/users/actionCreators";
import Login from "./pages/loginPage/Login";


const App = () => {


  const dispatch = useDispatch()
  const modal = useSelector(store => store.modal.value)
    const isLoadingUsers = useSelector(store => store.users.isLoading)



  useEffect(async  () => {
      dispatch(setUsersAC())
//dispatch(setAuthAC('a'))

  }, [])

    const authUser = useSelector(store => store.users.auth);


    let id1 = authUser?.id
  if (isLoadingUsers) {
    return (
      <>
       <h1>LOADING</h1>
      </>
    )
  }
  else if (authUser == null) {
        return (
            <>
                <div className='App'>
                    <Login/></div>
            </>
        )
    }
  else {
    return (
      <>
        <div className='App'>
            <Header id = {id1} />

          <AppRoutes authId = {id1} />

        </div>

      </>
    );
  }
}

export default App;