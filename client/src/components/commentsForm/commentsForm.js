import React from "react";
import { Field, Formik, Form, ErrorMessage } from "formik";
import { useDispatch, useSelector } from "react-redux";
import * as yup from 'yup'

import styles from './commentsForm.module.scss'




const CommentsForm = (props) => {
  const dispatch = useDispatch()


  let initialValues = {
    comment: '',

  }
  const validationSchema = yup.object().shape({
    comment: yup.string()
      .min(3, 'Min 3 symbols')
      .max(100, 'Max 100 symbols')
      .required('Text is required'),

  })
  return (

    <Formik
      initialValues={initialValues}

      validationSchema={validationSchema}
      onSubmit={async (values, FormikProps) => {}}

    >
      {({ dirty, isValid }) => {

        return (
          <Form >

            <Field className={styles.form}
              type='text'
              name='comment'
              placeholder='Add comment'

            />
            <ErrorMessage name="comment">{msg => <span className={styles.error}>{msg}</span>}</ErrorMessage>

            <button className={styles.form_button} disabled={!dirty || !isValid} type="submit">Post</button>

          </Form>

        )
      }
      }
    </Formik>
  )
}
export default CommentsForm
