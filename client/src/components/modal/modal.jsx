
import React from 'react'
import styles from './modal.module.scss'
import { useDispatch, useSelector } from 'react-redux'
import { closeModalAC } from '../../store/modal/actionCreators'

import ProfileForm from "../profileForm/ProfileForm";



const Modal = () => {

  const dispatch = useDispatch()


  return (


    <div className={styles.modal}>
      <div className={styles.outer_container} onClick={() => { dispatch(closeModalAC()) }}></div>
      <div className={styles.modal_main_container} >
 <h1 className={styles.form_title}>Edit profile </h1>

          <ProfileForm  />

          <button className={styles.close_btn} onClick={() => { dispatch(closeModalAC()) }}>X</button>

        </div>


    </div>

  )

}


export default Modal
