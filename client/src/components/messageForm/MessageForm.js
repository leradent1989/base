import React,{useRef} from "react";
import {useDispatch} from "react-redux";
import {setChatMessagesAC} from "../../store/chat/actionCreators";
import styles from './MessageForm.module.scss';


const MessageForm = (props) => {


const dispatch = useDispatch();
    const textRef = useRef(null);

    const handleSubmit = async (e) => {
        e.preventDefault();


      await  fetch(`http://localhost:9000/messages`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'},
            body: JSON.stringify({
                "id": 10,
                "sender": {
                    "id": `${props.senderId}`,
                    "creationDate": "2023-05-25T06:39:46.670Z",
                    "lastModifiedDate": "2023-05-25T06:39:46.670Z",
                    "name": "string",
                    "surname": "string",
                    "address": "string",
                    "birthDate": "2023-05-25T06:39:46.670Z",
                    "avatar": "string",
                    "header": "string",
                    "posts": []
                },
                "receiver": {
                    "id": `${props.receiverId}`,
                    "creationDate": "2023-05-25T06:39:46.670Z",
                    "lastModifiedDate": "2023-05-25T06:39:46.670Z",
                    "name": "string",
                    "surname": "string",
                    "address": "string",
                    "birthDate": "2023-05-25T06:39:46.670Z",
                    "avatar": "string",
                    "header": "string",
                    "posts": []
                },
                "text": `${textRef.current.value}`



            })
        })
await  dispatch(setChatMessagesAC(props.senderId,props.receiverId))
    }

    return (
        <form className={styles.message_form} onSubmit={(e) => handleSubmit(e)}>


            <input className={styles.message_input}
                type="text"
                name="id"
                placeholder="MessageText"
                ref={textRef}
            /><br/>


            <button className={styles.message_btn}  type="submit" >Send message</button>
        </form>
    )
}

export default MessageForm;