import React,{useRef} from "react";
import {useDispatch} from "react-redux";
import styles from "./ProfileForm.module.scss"
import {setUsersAC} from "../../store/users/actionCreators";



const ProfileForm = () => {


    const dispatch = useDispatch();
    const nameRef = useRef(null);
    const surnameRef = useRef(null);
    const birthRef = useRef(null);
    const adressRef = useRef(null);
    const handleSubmit = async (e) => {
        e.preventDefault();


        await  fetch(`http://localhost:9000/users`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'},
            body: JSON.stringify({
                name:`${nameRef.current.value}`,
                surname:`${surnameRef.current.value}`,
                address:`${adressRef.current.value}`,
                birthDate:`${birthRef.current.value}`,
                avatar:'',
                header:''

            })
        })
        await  dispatch(setUsersAC())
    }

    return (
        <>
        <form className={styles.profile_form} onSubmit={(e) => handleSubmit(e)}>


            <input className={styles.form_input}
                type="text"
                name="name"
                placeholder="Name"
                ref={nameRef}
            /><br/>
            <input className={styles.form_input}
                type="text"
                name="surname"
                placeholder="Surname"
                ref={surnameRef}
            /><br/>
            <input className={styles.form_input}
                type="text"
                name="birthdate"
                placeholder="Birth date"
                ref={birthRef}
            /><br/>
            <input className={styles.form_input}
                type="text"
                name="adress"
                placeholder="Adress"
                ref={adressRef}
            /><br/>

            <button className={styles.profile_form_btn} type="submit" >Edit profile</button>
        </form>
    </>
    )
}

export default ProfileForm;