import React,{useRef} from "react";
import {useDispatch} from "react-redux";

import styles from './PostForm.module.scss'
import {setPostsAC} from "../../store/posts/actionCreators";


const PostForm = (props) => {


    const dispatch = useDispatch();
    const textRef = useRef(null);

    const handleSubmit = async (e) => {
        e.preventDefault();


        await  fetch(`http://localhost:9000/posts`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'},
            body: JSON.stringify({
                "id": 10,
                "text": `${textRef.current.value}`,
                "author":props.author,
                "liked": false

            })
        })
        dispatch(setPostsAC())

    }

    return (
        <form className={styles.post_form} onSubmit={(e) => handleSubmit(e)}>


            <input className={styles.message_input}
                   type="text"
                   name="id"
                   placeholder="MessageText"
                   ref={textRef}
            /><br/>


            <button className={styles.post_btn} type="submit" >Add post</button>
        </form>
    )
}

export default PostForm;
/*{
    "id": `${props.authorId}`,
        "creationDate": "2023-06-06T12:40:51.667Z",
        "lastModifiedDate": "2023-06-06T12:40:51.667Z",
        "name": "string",
        "surname": "string",
        "address": "string",
        "birthDate": "2023-06-06T12:40:51.667Z",
        "avatar": "string",
        "header": "string",
        "posts": [
        {
            "id": 0,
            "creationDate": "2023-06-06T12:40:51.667Z",
            "lastModifiedDate": "2023-06-06T12:40:51.667Z",
            "text": "string",
            "liked": true  ]
        } }*/