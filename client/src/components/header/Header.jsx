import React from 'react'
import { NavLink } from 'react-router-dom'
import styles from './Header.module.scss'
import {useDispatch} from "react-redux";



const Header = ({id}) => {
const dispatch = useDispatch();

    return (
        <header className={styles.header}>
            <NavLink className={styles.profile_link} to='/' ><h2 >Posts</h2 ></NavLink>

                <NavLink className={styles.profile_link} to={`/${id}`}><h2>Profile</h2></NavLink>
                <NavLink className={styles.chat_link} to={`/${id}/chat`}> <h2>Chat</h2></NavLink>
                <NavLink className={styles.chat_link} to={`/${id}/notifications`}> <h2>Notifications</h2></NavLink>
                <NavLink className={styles.chat_link} to={`/${id}/favorites`}> <h2>Favorites</h2></NavLink>
                <NavLink className={styles.chat_link} to="/logout"> <h2>Logout</h2></NavLink>
        </header>)
}
export default Header;

// <button onClick={() =>{dispatch(logOutAC())}} >LogOut</button>