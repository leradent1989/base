import React, {useEffect, useState} from 'react';
import {useDispatch} from "react-redux";
import {likePostAC, setPostsAC} from "../../store/posts/actionCreators";
import styles from './Post.module.scss'
import {setFollowersAC} from "../../store/Followers/actionCreators";


const Post = ({name,surname,text,id,avatar,liked}) => {

const dispatch = useDispatch()
    const [fill,setFill] = useState('pink')
    useEffect( () => {

        if(liked){
            setFill('red')
        }
    }, [])



    return (
        <>
            <div className={styles.post_container} >
            <ul><li className={styles.author_container}><img className={styles.avatar} src={avatar}/>{name}{surname}</li><li className={styles.post_text}>{text}</li></ul>

                <svg onClick={() =>{
                    dispatch(likePostAC({id:id}))
                    setFill('red')
                }

                } className={styles.svg} version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px"
                     viewBox="0 0 32 32">
                    <g>
                        <g id="heart_x5F_fill"
                           fill={fill}
                        >
                            <g>
                                <g>
                                    <path  d="M16,5.844C14.387,3.578,11.871,2,8.887,2C3.984,2,0,5.992,0,10.891v0.734L16.008,30L32,11.625
                 v-0.734C32,5.992,28.016,2,23.113,2C20.129,2,17.613,3.578,16,5.844z"/>
                                </g>	</g>	</g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g>
                </g>
                </svg>


        </div>
        </>
    )

}
export default Post;