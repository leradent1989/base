import React, {useEffect} from 'react';

import {useDispatch, useSelector} from "react-redux";
import styles from './Profile.module.scss'
import {NavLink, useParams} from "react-router-dom";
import {setFollowersAC} from "../../store/Followers/actionCreators";
import {openModalAC} from "../../store/modal/actionCreators";
import Modal from "../../components/modal/modal";
import {addFollowerAC} from "../../store/Followers/actionCreators";
import {removeFollowerAC} from "../../store/Followers/actionCreators";


const Profile = ({auth}) => {
let {id} = useParams();
  const dispatch = useDispatch();
    useEffect(() => {
        dispatch(setFollowersAC(id))

    }, [])


    const users =useSelector(store => store.users.value)
    const followers = useSelector(store =>store.followers.value)
    let index = users.findIndex(el => el.id == id)
    let user = users[index];
    const modal = useSelector(store =>store.modal.value)

    const user2 = followers.filter(el=>el.id === +auth)[0]

    const follower = users.filter(el => el.id === +id)[0]
    console.log(follower)
    console.log(user2)
console.log(typeof +id)

  //  console.log(user.followers)
 //let followersId = user.followers.map(el => el.id);

   // console.log(followersId)

 // let followers = users.filter(el => followersId.includes(el.id));
  console.log(followers)

console.log(users)
     return (
          <>

             <div className={styles.profile_wrapper}>
                 {  modal &&
                     <Modal/>

                 }
                 <img className={styles.header} src={user.header}/>
                 <div className={styles.profile_container} >

                   <img className={styles.avatar} src={user.avatar}/>
                  <ul className={styles.profile}>

                      <li>{user.name}</li>
                      <li>{user.surname}</li>
                      <li>{user.address}</li>
                      <li>{user.birthDate.substring(0,10)}</li>
                      <li></li>
                  </ul>

                     {id == auth &&
                         <button className={styles.edit_btn} onClick={() => { dispatch(openModalAC()) }}>Edit profile</button>
                     }
                     {id != auth && followers.includes(user2) &&
                         <button className={styles.follower_btn} onClick={async () => {
                             dispatch(removeFollowerAC({followers:followers,follower:follower,auth:user2}))

                         }
                         }>Remove follower</button>
                     }
                     {id != auth && !followers.includes(user2) &&
                         <button className={styles.follower_btn} onClick={() => { dispatch(addFollowerAC({follower:follower,auth:user2})) }}>Add follower</button>
                     }
                 </div>

                 <h1>Followers:</h1>
                 <ol className={styles.follower_list}>
                    {followers.map(el => <li > <NavLink className = {styles.follower_link} onClick = {()=>{dispatch(setFollowersAC(el.id))}}  className={styles.follower_link}  to={`/${el.id}`}><img className={styles.follower_img} src={el.avatar}/><h3>{el.name} {el.surname}</h3></NavLink></li>)}
             </ol>
              </div>

          </>
     )

}
export default Profile ;

