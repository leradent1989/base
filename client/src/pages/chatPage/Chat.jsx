import React, {useEffect} from 'react';
import styles from "../chatPage/Chat.module.scss";
import {NavLink, useParams} from "react-router-dom";
import {setFollowersAC} from "../../store/Followers/actionCreators";
import {useDispatch, useSelector} from "react-redux";


const Chat = () => {
    let {id} = useParams();
    const dispatch = useDispatch();
    useEffect( () => {

        dispatch(setFollowersAC(id))

    }, [])

    const messages = useSelector(store => store.messages.value)
    const followers = useSelector(store =>store.followers.value)

    return (
        <>
   <div className={styles.chat_container}>
            <h1>Chats:</h1>
            <ol>
                {followers.map(el => <li> <NavLink className={styles.follower_link}  to={`/${id}/${el.id}/chat`}><h3 >{el.name} {el.surname}</h3></NavLink></li>)}
            </ol>
        </div>
        </>
    )

}
export default Chat;
//onClick = { ()=>{    dispatch(setChatMessagesAC(id,el.id))}}