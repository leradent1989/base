import React, {useEffect} from 'react';
import styles from "../chatPage/Chat.module.scss";
import {NavLink, useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {setChatMessagesAC} from "../../store/chat/actionCreators";
import MessageForm from "../../components/messageForm/MessageForm";


const ChatPage = () => {

  const {id,id2} = useParams();
  const dispatch = useDispatch();

    useEffect( () => {
        dispatch(setChatMessagesAC(id,id2))

    }, [])

    let messages = useSelector(store => store.messages.value)


 return(<>

     <div className={styles.chat_wrapper}>

         <ol className={styles.chat}>

             {messages.map(el => <ul><li className={styles.sender}>{el.sender.name}{el.sender.surname}</li><li className={styles.text} >{el.text}</li></ul>)
             }
         </ol>
         <MessageForm   senderId = {id} receiverId = {id2}/>
     </div>


 </>)


}

export default ChatPage;