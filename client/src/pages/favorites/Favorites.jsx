import React, {useEffect} from 'react';
import {setPostsAC} from "../../store/posts/actionCreators";
import {useDispatch, useSelector} from "react-redux";
import styles from "../postsPage/Posts.module.scss";
import Favorite from "../../components/favorite/Favorite";



const Favorites = () => {
    const  dispatch = useDispatch();

const posts = useSelector(store => store.posts.value);
    const favorites = posts.filter(el => el.liked === true);

    useEffect( () => {
        dispatch(setPostsAC())

    }, [favorites])

    return (
        <>

            <div className={styles.post_page_container} >

                {favorites.map(el => <Favorite index={el.id} id={el.id} name = {el.author.name} surname = {el.author.surname} avatar={el.author.avatar} text ={el.text} />  )}
            </div>

        </>
    )

}
export default Favorites;