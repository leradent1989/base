import React, { useEffect,useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {setPostsAC} from "../../store/posts/actionCreators";
import Post from "../../components/post/Post";
import styles from './Posts.module.scss'
import {setFollowersAC} from "../../store/Followers/actionCreators";
import PostForm from "../../components/postForm/PostForm";


const Posts = ({id}) => {

  const dispatch = useDispatch()


  useEffect( () => {
      dispatch(setPostsAC())
      dispatch(setFollowersAC(id))
  }, [])

    const posts = useSelector(store =>store.posts.value)
    const followers = useSelector(store =>store.followers.value)
    const users = useSelector(store =>store.users.value)
    const author  = users.filter(el => el.id == id)[0];

    const idArr = followers.map(el=> el = el.id)
    idArr.push(id)

    const userPosts = posts.filter(el =>idArr.includes(el.author.id))


  const scrollHandler = (e) => {
    if (e.target.documentElement.scrollTop + window.innerHeight === e.target.documentElement.scrollHeight) {

  }
  }

  useEffect(() => {
    document.addEventListener('scroll', scrollHandler)

    return function () {

      document.removeEventListener('scroll', scrollHandler)
    }
  }, [])

  return (
    <>
      <div className={styles.post_page_container} >

          {userPosts.map(el => <Post index={el.id} id={el.id} name = {el.author.name} surname = {el.author.surname} avatar ={el.author.avatar} text ={el.text} liked ={el.liked} />  )}
          <PostForm author = {author} />
      </div>

    </>
  )
}
export default Posts

           