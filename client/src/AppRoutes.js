import React from 'react'
import { Routes, Route, useParams,Outlet } from 'react-router-dom'
import Profile from './pages/userPage/Profile';
import NotFoundPage from './pages/notfoundpage/notfoundpage';
import Posts from './pages/postsPage/Posts';
import Notification from "./pages/notificationPage/Notification";
import Chat from "./pages/chatPage/Chat";
import Favorites from "./pages/favorites/Favorites";
import Login from "./pages/loginPage/Login";
import ChatPage from "./pages/chatPage/ChatPage";

const AppRoutes = ({authId}) => {

   

  //  let {nickName } =  useParams();
    let {id} =  useParams();

    return (

        <Routes>
          
            <Route path='/' element={<Outlet />} >
             <Route index element = {<Posts id={authId} />}/>
           <Route  path={"/:id"} element={<Profile nickName ={id}  auth ={authId} />} />
          </Route>
            <Route path={"/:id/notifications"} element={<Notification id = {id} />} />
            <Route path={"/:id/chat"} element={<Chat id = {id} />} />
            <Route path={"/:id/:id2/chat"} element={<ChatPage  />} />
            <Route path={"/:id/favorites"} element={<Favorites id = {id} />} />
            <Route path={"/logout"} element={<Login  />} />
            <Route path='*' element={<NotFoundPage />} />
        </Routes>

    )
}
export default AppRoutes; 
